﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Lib.WebClient
{
    /// <summary>
    /// Базовый менеджер данных
    /// </summary>
    public abstract class BaseManager
    {
        /// <summary>
        /// Клиент для связи с API
        /// </summary>
        protected static HttpClient client = new HttpClient(new HttpClientHandler() {
             UseDefaultCredentials = true
        });

        /// <summary>
        /// Префикс API
        /// </summary>
        protected abstract string RoutePrefix { get; }   
        
        /// <summary>
        /// Создать путь с использованием префикса
        /// </summary>
        protected string Route(string subroute = null)
        {
            if (subroute == null) return RoutePrefix;
            return $"{RoutePrefix}/{subroute}";
        }

        /// <summary>
        /// Загрузить настройки соединения для менеджера данных
        /// </summary>
        public static void Load(string server, string port)
        {
            if (server == null || port == null)
            {
                client = null;
                return;
            }
            client.BaseAddress = new Uri($"http://{server}:{port}/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
    }
}
