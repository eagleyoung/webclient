﻿namespace Lib.WebClient
{
    /// <summary>
    /// Базовая модель представления объекта
    /// </summary>
    public class BaseManagerItem
    {
        /// <summary>
        /// Идентификатор объекта
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Токен для обновления объекта
        /// </summary>
        public long UpdateToken { get; set; }
    }
}
