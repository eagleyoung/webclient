﻿using Lib.Shared;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Lib.WebClient
{
    public static class Extensions
    {
        class Response
        {
            public string Message { get; set; }
            public Dictionary<string, string[]> ModelState { get; set; }
        }

        /// <summary>
        /// Вывести ошибку при выполнении запроса
        /// </summary>
        public static async Task OutputResponseErrorAsync(this HttpResponseMessage response)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.Unauthorized:
                    Output.WarningAdditional("Пользователь не авторизован", response.ReasonPhrase);
                    break;
                case HttpStatusCode.InternalServerError:
                    Output.ErrorAdditional("Ошибка на сервере", response.ReasonPhrase);
                    break;
                case HttpStatusCode.BadRequest:
                    bool traced = false;
                    var errors = await response.Content.ReadAsAsync<Response>();
                    foreach(var pair in errors.ModelState)
                    {
                        foreach(var error in pair.Value)
                        {
                            Output.ErrorAdditional(error, errors.Message);
                            traced = true;
                        }
                    }
                    if (!traced)
                    {
                        Output.ErrorAdditional("Ошибка при выполнении запроса", response.ReasonPhrase);
                    }
                    break;
                default:
                    Output.ErrorAdditional("Ошибка при выполнении запроса", response.ReasonPhrase);                    
                    break;
            }
        }
    }
}
